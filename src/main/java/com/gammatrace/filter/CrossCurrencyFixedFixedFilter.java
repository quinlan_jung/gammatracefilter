/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filter;

import java.util.Map;
import java.util.Map.Entry;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;

public class CrossCurrencyFixedFixedFilter extends AbstractFilter{

	public CrossCurrencyFixedFixedFilter() {
		super(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED);
	}
	
	@Override
	public boolean dtccValueMeetsCriteria(DtccValue dtccValue, Object value, Map<String, Object> trade){
		switch(dtccValue){
		case END_DATE:
			return endDateCheck(dtccValue, value, trade);
		case SETTLEMENT_CURRENCY:
			return settlementCurrencyCheck(dtccValue, (String) value, trade);
		case ADDITIONAL_PRICE_NOTATION_TYPE:
			return additionalPriceNotationTypeCheck(dtccValue, value, trade);
		case ADDITIONAL_PRICE_NOTATION:
			return additionalPriceNotationCheck(dtccValue, value, trade);
		case PRICE_NOTATION2:
			return priceNotation2Check(dtccValue, value, trade);
		case PRICE_NOTATION3_TYPE:
			return priceNotation3TypeCheck(dtccValue, value, trade);
		case PRICE_NOTATION3:
			return priceNotation3Check(dtccValue, value, trade);
		default:
			return staticValueCheck(dtccValue, value);
		}
	}
	
	/**
	 * If AdditionalPriceNotationType is not blank, it must be equal to notional currency 1 or 2
	 * @param dtccValue: AdditionalPriceNotationType
	 * @param value: either a valid double, or null
	 * @param trade: the entire trade
	 *
	 */
	protected boolean additionalPriceNotationTypeCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		String additionalPriceNotationTypeValue = (String) value;
		String notionalCurrency1 = (String) trade.get(DtccValue.NOTIONAL_CURRENCY_1.toString());
		String notionalCurrency2 = (String) trade.get(DtccValue.NOTIONAL_CURRENCY_2.toString());
		if (!additionalPriceNotationTypeValue.equals(EMPTY_STRING)){
			return (additionalPriceNotationTypeValue.equals(notionalCurrency1) || additionalPriceNotationTypeValue.equals(notionalCurrency2)) && staticValueCheck(dtccValue, value);
		}
		return staticValueCheck(dtccValue, value);
	}
	
	/**
	 * PriceNotation3Type must be equal to either NotionalCurrency1 or 2 and must pass staticMethodChecks
	 * Else, PriceNotation3Type must be blank (pass staticMethodChecks)
	 * @param dtccValue: PriceNotation3Type
	 * @param value: PriceNotation3Type's string value
	 * @param trade: the entire trade
	 * @return true if PriceNotation3Type is equal to either NotionalCurrency1 or 2 and passes staticMethodChecks
	 *         true if PriceNotation3Type is blank
	 *
	 */
	private boolean priceNotation3TypeCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		if (!value.equals(EMPTY_STRING)){
			String notionalCurrency1 = (String) trade.get(DtccValue.NOTIONAL_CURRENCY_1.toString());
			String notionalCurrency2 = (String) trade.get(DtccValue.NOTIONAL_CURRENCY_2.toString());
			return (notionalCurrency1.equals(value) || notionalCurrency2.equals(value)) && staticValueCheck(dtccValue, value);
		}
		return true;
	}
	
	/**
	 * If PriceNotationType3 is not blank, then PriceNotation3 must be a valid double
	 * Else, PriceNotation3 must be null
	 * @param dtccValue: PriceNotation3
	 * @param value: either a valid double, or null
	 * @param trade: the entire trade
	 * @return true if PriceNotationType3 is not blank, and PriceNotation3 is a valid double
	 *         true if PriceNotationType3 is blank, and PriceNotation3 is null
	 *         false otherwise
	 *
	 */
	private boolean priceNotation3Check(DtccValue dtccValue, Object value, Map<String, Object> trade){
		String priceNotation3TypeValue = (String) trade.get(DtccValue.PRICE_NOTATION3_TYPE.toString());
		if (!priceNotation3TypeValue.equals(EMPTY_STRING)){
			return !isNull(value);
		}
		return isNull(value);
	}
}
