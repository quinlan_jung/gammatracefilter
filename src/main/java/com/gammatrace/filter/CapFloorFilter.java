/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filter;

import java.util.Map;

import com.gammatrace.commonutils.Utils;
import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;

public class CapFloorFilter extends AbstractFilter{
	public CapFloorFilter() {
		super(Taxonomy.INTERESTRATE_CAPFLOOR);
	}

	@Override
	public boolean dtccValueMeetsCriteria(DtccValue dtccValue, Object value, Map<String, Object> trade){
		switch(dtccValue){
		case END_DATE:
			return endDateCheck(dtccValue, value, trade);
		case SETTLEMENT_CURRENCY:
			return settlementCurrencyCheck(dtccValue, (String) value, trade);
		case NOTIONAL_CURRENCY_1:
			return notionalCurrencyCheck(dtccValue, (String) value, trade);
		case RESET_FREQUENCY_1:
		case RESET_FREQUENCY_2:
			return resetFrequencyCheck(dtccValue, (String) value, trade);
		case OPTION_CURRENCY:
			return optionCurrencyCheck(dtccValue, value, trade);
		case OPTION_STRIKE_PRICE:
			return optionStrikePriceCheck(dtccValue, value, trade);
		default:
			return staticValueCheck(dtccValue, value);
		}
	}
	
	
	/**
	 * ResetFrequency1 must equal PaymentFrequency1 and pass the static value check
	 * ResetFrequency2 must be blank (as specified in the static value check)
	 * @param dtccValue: ResetFrequency1 or ResetFrequency2
	 * @param value: ResetFrequency's string value
	 * @param trade: the entire trade
	 * @return true if ResetFrequency1 equals PaymentFrequency1 and passes the static value check
	 *         true if ResetFrequency2 is blank (as specified in the static value check)
	 *         false otherwise
	 *
	 */
	@Override
	protected boolean resetFrequencyCheck(DtccValue dtccValue, String value, Map<String, Object> trade){
		switch(dtccValue){
		case RESET_FREQUENCY_1:
			String paymentFrequency1 = DtccValue.PAYMENT_FREQUENCY_1.toString();
			return value.equals(trade.get(paymentFrequency1)) && staticValueCheck(dtccValue, value);
		case RESET_FREQUENCY_2:
			return staticValueCheck(dtccValue, value);
		default: // The wrong dtccValue has been passed in
			return false;
		}
	}
	
	/**
	 * OptionStrikePrice must be equivalent to PriceNotation / 100 and must be a valid double
	 * @param dtccValue: OptionStrikePrice
	 * @param value: either a valid double, or null
	 * @param trade: the entire trade
	 * @return true if OptionStrikePrice is equivalent to PriceNotation / 100 and is a valid double
	 *         false otherwise
	 *
	 */
	private boolean optionStrikePriceCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		double epsilon = 0.01d;
		Object priceNotation = trade.get(DtccValue.PRICE_NOTATION.toString());
		return !isNull(value) && !isNull(priceNotation) && Utils.fuzzyEquals((Double) priceNotation/100d, (Double) value, epsilon);
	}
}
