/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filter;

import java.util.Map;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;

public class CrossCurrencyBasisFilter extends AbstractFilter{

	public CrossCurrencyBasisFilter() {
		super(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS);
	}
	
	@Override
	public boolean dtccValueMeetsCriteria(DtccValue dtccValue, Object value, Map<String, Object> trade){
		switch(dtccValue){
		case END_DATE:
			return endDateCheck(dtccValue, value, trade);
		case SETTLEMENT_CURRENCY:
			return settlementCurrencyCheck(dtccValue, (String) value, trade);
		case PRICE_NOTATION_TYPE:
			return priceNotationTypeCheck(dtccValue, value, trade);
		case PRICE_NOTATION:
			return priceNotationCheck(dtccValue, value, trade);
		case ADDITIONAL_PRICE_NOTATION_TYPE:
			return additionalPriceNotationTypeCheck(dtccValue, value, trade);
		case ADDITIONAL_PRICE_NOTATION:
			return additionalPriceNotationCheck(dtccValue, value, trade);
		case PRICE_NOTATION2:
			return priceNotation2Check(dtccValue, value, trade);
		default:
			return staticValueCheck(dtccValue, value);
		}
	}
}
