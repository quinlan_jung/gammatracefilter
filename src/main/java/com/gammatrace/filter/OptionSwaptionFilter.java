/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filter;

import java.util.Map;
import java.util.Map.Entry;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;

public class OptionSwaptionFilter extends AbstractFilter{

	public OptionSwaptionFilter() {
		super(Taxonomy.INTERESTRATE_OPTION_SWAPTION);
	}
	
	@Override
	public boolean dtccValueMeetsCriteria(DtccValue dtccValue, Object value, Map<String, Object> trade){
		switch(dtccValue){
		case END_DATE:
			return endDateCheck(dtccValue, value, trade);
		case ADDITIONAL_PRICE_NOTATION:
			return additionalPriceNotationCheck(dtccValue, value, trade);
		case NOTIONAL_CURRENCY_1:
		case NOTIONAL_CURRENCY_2:
			return notionalCurrencyCheck(dtccValue, (String) value, trade);
		case RESET_FREQUENCY_1:
		case RESET_FREQUENCY_2:
			return resetFrequencyCheck(dtccValue, (String) value, trade);
		case OPTION_STRIKE_PRICE:
			return optionStrikePriceCheck(dtccValue, value, trade);
		case OPTION_CURRENCY:
			return optionCurrencyCheck(dtccValue, value, trade);
		case OPTION_PREMIUM:
			return optionPremiumCheck(dtccValue, value, trade);
		case PRICE_NOTATION2:
			return priceNotation2Check(dtccValue, value, trade);
		default:
			return staticValueCheck(dtccValue, value);
		}
	}
	
	/**
	 * If AdditionalPriceNotationType is not blank, then AdditionalPriceNotation must be a valid double
	 * Else, AdditionalPriceNotation must be null
	 * @param dtccValue: AdditionalPriceNotation
	 * @param value: either a valid double, or null
	 * @param trade: the entire trade
	 * @return true if AdditionalPriceNotationType is not blank, and AdditionalPriceNotation is a valid double
	 *         true if AdditionalPriceNotationType is blank, and AdditionalPriceNotation is null
	 *         false otherwise
	 *
	 */
	protected boolean additionalPriceNotationCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		String additionalPriceNotationTypeValue = (String) trade.get(DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE.toString());
		Double optionPremium = (Double) trade.get(DtccValue.OPTION_PREMIUM.toString());
		if (!additionalPriceNotationTypeValue.equals(EMPTY_STRING) && isNull(optionPremium)){
			return !isNull(value);
		}
		return isNull(value);
	}

	protected boolean resetFrequencyCheck(DtccValue dtccValue, String value, Map<String, Object> trade){
		if (dtccValue.equals(DtccValue.RESET_FREQUENCY_1) && !isLeg1Fixed(trade)){
			String paymentFrequency1 = (String) trade.get(DtccValue.PAYMENT_FREQUENCY_1.toString());
			return value.equals(paymentFrequency1) && super.resetFrequencyCheck(dtccValue, value, trade);
		}
		if (dtccValue.equals(DtccValue.RESET_FREQUENCY_2) && !isLeg2Fixed(trade)){
			String paymentFrequency2 = (String) trade.get(DtccValue.PAYMENT_FREQUENCY_2.toString());
			return value.equals(paymentFrequency2) && super.resetFrequencyCheck(dtccValue, value, trade);
		}
		return super.resetFrequencyCheck(dtccValue, value, trade);
	}
	
	/**
	 * OptionStrikePrice must be equal to PriceNotation and a valid double
	 * @param dtccValue: OptionStrikePrice
	 * @param value: either a valid double, or null
	 * @param trade: the entire trade
	 * @return true if OptionStrikePrice is equal to PriceNotation and is a valid double
	 *         false otherwise
	 *
	 */
	private boolean optionStrikePriceCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		Object priceNotation = trade.get(DtccValue.PRICE_NOTATION.toString());
		return !isNull(value) && !isNull(priceNotation) && priceNotation.equals(value);
	}
	
	/**
	 * OptionPremium must be a valid double if OptionCurrency is not blank
	 * Else OptionPremium must be null
	 * @param dtccValue: OptionPremium
	 * @param value: either a valid double, or null
	 * @param trade: the entire trade
	 * @return true if OptionPremium is a valid double and OptionCurrency is not blank
	 *         true if OptionPremium is null and OptionCurrency is blank
	 *         false otherwise
	 *
	 */
	private boolean optionPremiumCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		String optionCurrency = (String) trade.get(DtccValue.OPTION_CURRENCY.toString());
		Double additionalPriceNotation = (Double) trade.get(DtccValue.ADDITIONAL_PRICE_NOTATION.toString());
		return (!optionCurrency.equals(EMPTY_STRING) && isNull(additionalPriceNotation) && !isNull(value)) || isNull(value);
	}
}
