/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filter;

import java.util.Map;

import com.gammatrace.commonutils.Constants;
import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;
import com.gammatrace.rules.AllowedRules;
import com.gammatrace.rules.IllegalRules;
import com.gammatrace.rules.NullRules;

/**
 * Contains all the common filtering methods
 *
 */
public abstract class AbstractFilter {
	final static String EMPTY_STRING = Constants.EMPTY_STRING;
	final static String FIXED = "FIXED";
	Taxonomy taxonomy;
	
	public AbstractFilter(Taxonomy taxonomy){
		this.taxonomy = taxonomy;
	}
	
	/**
	 * Check if a value passes null checks. If it is a string, perform additional allowed/illegal checks.
	 * @param dtccValue: The column we are performing the check on
	 * @param value: the value of the column
	 * @return true if value passes static checks, false otherwise
	 *
	 */
	public boolean staticValueCheck(DtccValue dtccValue, Object value){
		if (NullRules.passesNullCheck(taxonomy, dtccValue, value)){
			if (value instanceof String){ // TODO: does this actually work?
				boolean containsAllowedValues = AllowedRules.containsAllowedValues(taxonomy, dtccValue, (String) value);
				boolean containsIllegalValues = IllegalRules.containsIllegalValues(taxonomy, dtccValue, (String) value);
				return containsAllowedValues && !containsIllegalValues;
			}
			return true;
		}
		return false;
	}
	
	/**
	 * @param dtccValue: ResetFrequency1
	 * @param trade: the entire trade
	 * @return true if ResetFrequency1 is fixed, false otherwise
	 *
	 */
	protected boolean isLeg1Fixed(Map<String, Object> trade){
		return isLegFixed(DtccValue.UNDERLYING_ASSET_1, trade);
	}
	
	/**
	 * @param dtccValue: ResetFrequency2
	 * @param trade: the entire trade
	 * @return true if ResetFrequency2 is fixed, false otherwise
	 *
	 */
	protected boolean isLeg2Fixed(Map<String, Object> trade){
		return isLegFixed(DtccValue.UNDERLYING_ASSET_2, trade);
	}
	
	/**
	 * @param dtccValue: Underlying Asset
	 * @param trade: the entire trade
	 * @return true if Underlying Asset is fixed, false otherwise
	 *
	 */
	private boolean isLegFixed(DtccValue dtccValue, Map<String, Object> trade){
		 String value = (String) trade.get(dtccValue.toString());
		 return value.equalsIgnoreCase(FIXED);
	}
	
	/**
	 * If the ResetFrequency is of the fixed leg, it must be blank
	 * Else, it must pass the staticValueCheck
	 * @param dtccValue: ResetFrequency1 or ResetFrequency2
	 * @param value: ResetFrequency's string value
	 * @param trade: the entire trade
	 * @return true if ResetFrequency is of the fixed leg, and it is blank
	 *         true if ResetFrequency is not of the fixed leg, and it passes the static value check
	 *         false otherwise
	 *
	 */
	protected boolean resetFrequencyCheck(DtccValue dtccValue, String value, Map<String, Object> trade){
		boolean isLegFixed;
		switch(dtccValue){
		case RESET_FREQUENCY_1:
			isLegFixed = isLeg1Fixed(trade);
			break;
		case RESET_FREQUENCY_2:
			isLegFixed = isLeg2Fixed(trade);
			break;
		default: // The wrong dtccValue has been passed in
			return false;
		}
		
		if (isLegFixed){
			return value.equals(EMPTY_STRING);
		}
		return staticValueCheck(dtccValue, value);
	}
	
	/**
	 * SettlementCurrency must be equal to one of the NotionalCurrencies and be a valid currency
	 * @param dtccValue: SettlementCurrency
	 * @param value: SettlementCurrency's string value
	 * @param trade: the entire trade
	 * @return true if SettlementCurrency is equal to on of the NotionalCurrencies and is a valid currency
	 *         false otherwise
	 *
	 */
	protected boolean settlementCurrencyCheck(DtccValue dtccValue, String value, Map<String, Object> trade){
		String notionalCurrency1 = (String) trade.get(DtccValue.NOTIONAL_CURRENCY_1.toString());
		String notionalCurrency2 = (String) trade.get(DtccValue.NOTIONAL_CURRENCY_2.toString());
		return (value.equals(notionalCurrency1) ||  value.equals(notionalCurrency2)) && staticValueCheck(dtccValue, value);
	}
	
	/**
	 * NotionalCurrency must be equal to SettlementCurrency and be a valid currency
	 * @param dtccValue: NotionalCurrency1 or NotionalCurrency2
	 * @param value: NotionalCurrency1 or NotionalCurrency2's string value
	 * @param trade: the entire trade
	 * @return true if NotionalCurrency is equal to SettlementCurrency and is a valid currency
	 *         false otherwise
	 *
	 */
	protected boolean notionalCurrencyCheck(DtccValue dtccValue, String value, Map<String, Object> trade){
		String settlementCurrency = (String) trade.get(DtccValue.SETTLEMENT_CURRENCY.toString());		
		return settlementCurrency.equals(value) && staticValueCheck(dtccValue, value);
	}
	
	/**
	 * OptionCurrency must be equal to SettlementCurrency and pass staticMethodChecks if not blank
	 * Else, OptionCurrency is blank and must pass staticMethodChecks
	 * @param dtccValue: OptionCurrency
	 * @param value: OptionCurrency2's string value
	 * @param trade: the entire trade
	 * @return true if OptionCurrency is equal to SettlementCurrency if not blank and pass staticMethodChecks
	 *         true if OptionCurrency is blank and passes staticMethodChecks
	 *         false otherwise
	 *
	 */
	protected boolean optionCurrencyCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		if (!value.equals(EMPTY_STRING)){
			String settlementCurrency = (String) trade.get(DtccValue.SETTLEMENT_CURRENCY.toString());
			return settlementCurrency.equals(value) && staticValueCheck(dtccValue, value);
		}
		return staticValueCheck(dtccValue, value);
	}
	
	/**
	 * If AdditionalPriceNotationType is not blank, it must be equal to a valid settlement currency
	 * @param dtccValue: AdditionalPriceNotationType
	 * @param value: either a valid double, or null
	 * @param trade: the entire trade
	 *
	 */
	protected boolean additionalPriceNotationTypeCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		String additionalPriceNotationTypeValue = (String) value;
		String settlementCurrency = (String) trade.get(DtccValue.SETTLEMENT_CURRENCY.toString());
		if (!additionalPriceNotationTypeValue.equals(EMPTY_STRING)){
			return additionalPriceNotationTypeValue.equals(settlementCurrency) && staticValueCheck(dtccValue, value);
		}
		return staticValueCheck(dtccValue, value);
	}
	
	/**
	 * If AdditionalPriceNotationType is not blank, then AdditionalPriceNotation must be a valid double
	 * Else, AdditionalPriceNotation must be null
	 * @param dtccValue: AdditionalPriceNotation
	 * @param value: either a valid double, or null
	 * @param trade: the entire trade
	 * @return true if AdditionalPriceNotationType is not blank, and AdditionalPriceNotation is a valid double
	 *         true if AdditionalPriceNotationType is blank, and AdditionalPriceNotation is null
	 *         false otherwise
	 *
	 */
	protected boolean additionalPriceNotationCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		String additionalPriceNotationTypeValue = (String) trade.get(DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE.toString());
		if (!additionalPriceNotationTypeValue.equals(EMPTY_STRING)){
			return !isNull(value);
		}
		return isNull(value);
	}
	
	/**
	 * If PriceNotationType2 is not blank, then PriceNotation2 must be a valid double
	 * Else, PriceNotation2 must be null
	 * @param dtccValue: PriceNotation2
	 * @param value: either a valid double, or null
	 * @param trade: the entire trade
	 * @return true if PriceNotationType2 is not blank, and PriceNotation2 is a valid double
	 *         true if PriceNotationType2 is blank, and PriceNotation2 is null
	 *         false otherwise
	 *
	 */
	protected boolean priceNotation2Check(DtccValue dtccValue, Object value, Map<String, Object> trade){
		String priceNotation2TypeValue = (String) trade.get(DtccValue.PRICE_NOTATION2_TYPE.toString());
		if (!priceNotation2TypeValue.equals(EMPTY_STRING)){
			return !isNull(value);
		}
		return isNull(value);
	}
	
	/**
	 * @return true if endDate greater than effectiveDate
	 *
	 */
	protected boolean endDateCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		Long effectiveDate = (Long) trade.get(DtccValue.EFFECTIVE_DATE.toString());
		if (effectiveDate == null | value == null){
			return false;
		}
		return ((Long) value) > effectiveDate;
	}
	
	/**
	 * Additional logic with the Blank case for basis swaps
	 * @return true if blank case has non-blank pricenotation2 type
	 *
	 */
	protected boolean priceNotationTypeCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		String priceNotationType = (String) value;
		String priceNotation2Type = (String) trade.get(DtccValue.PRICE_NOTATION2_TYPE.toString());
		if (priceNotationType.isEmpty() && priceNotation2Type.isEmpty()){
			return false;
		}
		return staticValueCheck(dtccValue, value);
	}
	
	/**
	 * Additional logic with the null case for basis swaps
	 * @return true if null case has non-blank pricenotation2 type
	 *
	 */
	protected boolean priceNotationCheck(DtccValue dtccValue, Object value, Map<String, Object> trade){
		Double priceNotation = (Double) value;
		String priceNotation2Type = (String) trade.get(DtccValue.PRICE_NOTATION2_TYPE.toString());
		if (priceNotation == null && priceNotation2Type.isEmpty()){
			return false;
		}
		return staticValueCheck(dtccValue, value);
	}
	
	protected boolean isNull(Object value){
		return value == null;
	}
	
	/**
	 * @return true if the dtccValue passes static and/or additional logic checks
	 */
	abstract public boolean dtccValueMeetsCriteria(DtccValue dtccValue, Object value, Map<String, Object> trade);
}
