/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filtermanager;

import java.util.Map;
import java.util.Map.Entry;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.filter.AbstractFilter;

/**
 * The Filter Manager we use in production
 */
public class ProductionFilterManager extends AbstractFilterManager{
	@Override
	public boolean tradeMeetsCriteria(AbstractFilter filter, Map<String, Object> trade) {
		for (Entry<String, Object> entry : trade.entrySet()){
			DtccValue dtccValue = DtccValue.toEnum(entry.getKey());
			boolean meetsCriteria = filter.dtccValueMeetsCriteria(dtccValue, entry.getValue(), trade);
			if (!meetsCriteria){
				return false;
			}
		}
		return true;
	}
}
