/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filtermanager;

import java.util.Map;

import com.gammatrace.filter.AbstractFilter;
/**
 * The Filter Manager that doesnt allow anything to be repriced
 */
public class MockFilterManager extends AbstractFilterManager{

	@Override
	public boolean tradeMeetsCriteria(AbstractFilter filter, Map<String, Object> trade) {
		return false;
	}

}
