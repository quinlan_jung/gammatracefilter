/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filtermanager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.filter.AbstractFilter;

/**
 * The filter manager for debugging our repricing criteria
 */
public class DebugFilterManager extends AbstractFilterManager{
	final Map<DtccValue, Set<Map<String, Object>>> rejectedTrades = new HashMap<DtccValue, Set<Map<String, Object>>> ();
	final Set<Map<String, Object>> acceptedTrades = new HashSet<Map<String, Object>> ();
	String mode = "STDOUT";
	BufferedWriter writer;

	@Override
	public boolean tradeMeetsCriteria(AbstractFilter filter, Map<String, Object> trade) {
		for (Entry<String, Object> entry : trade.entrySet()){
			DtccValue dtccValue = DtccValue.toEnum(entry.getKey());
			boolean meetsCriteria = filter.dtccValueMeetsCriteria(dtccValue, entry.getValue(), trade);
			if (!meetsCriteria){
				addTrade(rejectedTrades, dtccValue, trade);
				return false;
			}
		}
		acceptedTrades.add(trade);
		return true;
	}
	
	/**
	 * Add a trade to the map
	 */
	private void addTrade(Map<DtccValue, Set<Map<String, Object>>> trades, DtccValue dtccValue, Map<String, Object> trade){
		Set<Map<String, Object>> tradesSet;
		if (!trades.containsKey(dtccValue)){
			tradesSet = new HashSet<Map<String, Object>> ();
			trades.put(dtccValue, tradesSet);
		}
		else{
			tradesSet = trades.get(dtccValue);
		}
		tradesSet.add(trade);
	}
	
	public void clearAllTrades(){
		rejectedTrades.clear();
		acceptedTrades.clear();
	}
	
	public void printToFile(Path p) throws IOException{
		Files.createDirectories(p.getParent());
		writer = Files.newBufferedWriter(p, Charset.defaultCharset());
		mode = "FILE";
		printResults();
		mode = "STDOUT";
	}
	
	public void printResults(){
		printSummary();
		printRejectedTrades();
		printAcceptedTrades();
	}
	
	public void printSummary(){
		int totalAccepted = printSummaryAccepted();
		int totalRejected = printSummaryRejected();
		int totalTrades = totalAccepted + totalRejected;
		double acceptanceRate = 100 * (double) totalAccepted / (double) totalTrades;
		double rejectionRate = 100 * (double) totalRejected / (double) totalTrades;
		print("=======================================");
		print("Total Trades: " + totalTrades);
		print(String.format("Acceptance Rate: %.2f", acceptanceRate));
		print(String.format("Rejection Rate: %.2f", rejectionRate));
	}
	
	private int printSummaryAccepted(){
		int totalAccepted = getNumber(acceptedTrades);
		print("Total Accepted Trades: " + totalAccepted);
		return totalAccepted;
	}
	
	private int printSummaryRejected(){
		int totalRejected = 0;
		for (Entry<DtccValue, Set<Map<String, Object>>> entry : rejectedTrades.entrySet()){
			Set<Map<String, Object>> trades = entry.getValue();
			int totalRejectedForColumn = getNumber(trades);
			totalRejected += totalRejectedForColumn;
			
			print(entry.getKey().toString() + " : " + totalRejectedForColumn);
		}
		print("Total Rejected Trades: "+totalRejected);
		return totalRejected;
	}
	
	private int getNumber(Set<Map<String, Object>> trades){
		return trades.size();
	}
	
	private void printHeader(){
		StringBuilder sb = new StringBuilder("");
		String delimiter = "";
		for (DtccValue dtccValue : DtccValue.PULLED_COLUMN_ARRAY){
			sb.append(delimiter);
			sb.append(dtccValue.toString());
			delimiter = ",";
		}
		print(sb.toString());
	}
	
	private void printRejectedTrades(){
		print("Rejected Trades:");
		for (Entry<DtccValue, Set<Map<String, Object>>> entry : rejectedTrades.entrySet()){
			print("Rejected Column: "+entry.getKey().toString());
			printHeader();
			Set<Map<String, Object>> trades = entry.getValue();
			for (Map<String, Object> trade : trades){
				printTrade(trade);
			}
		}
	}
	
	private void printAcceptedTrades(){
		print("Accepted Trades:");
		printHeader();
		for (Map<String, Object> trade : acceptedTrades){
			printTrade(trade);
		}
	}
	
	private void printTrade(Map<String, Object> trade){
		StringBuilder sb = new StringBuilder("");
		String delimiter = "";
		for (DtccValue dtccValue : DtccValue.PULLED_COLUMN_ARRAY){
			sb.append(delimiter);
			sb.append(trade.get(dtccValue.toString()));
			delimiter = ",";
		}
		print(sb.toString());
	}
	
	public Map<DtccValue, Set<Map<String, Object>>> getRejectedtrades() {
		return rejectedTrades;
	}

	public Set<Map<String, Object>> getAcceptedtrades() {
		return acceptedTrades;
	}
	
	/**
	 * Print to STDOUT or FILE
	 */
	private void print(String s){
		if (mode.equals("STDOUT")){
			System.out.println(s);
		}
		else if (mode.equals("FILE")){
			try {
				writer.append(s);
				writer.newLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
}
