/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filtermanager;

import java.util.Map;
import java.util.Set;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;
import com.gammatrace.filter.AbstractFilter;
import com.gammatrace.filter.CapFloorFilter;
import com.gammatrace.filter.CrossCurrencyBasisFilter;
import com.gammatrace.filter.CrossCurrencyFixedFixedFilter;
import com.gammatrace.filter.CrossCurrencyFixedFloatFilter;
import com.gammatrace.filter.IRSwapBasisFilter;
import com.gammatrace.filter.IRSwapFixedFloatFilter;
import com.gammatrace.filter.IRSwapOISFilter;
import com.gammatrace.filter.OptionSwaptionFilter;

/**
 * Determines whether a trade should be repriced by calling filters
 *
 */
public abstract class AbstractFilterManager {
	
	/**
	 * Determines whether a trade should be repriced
	 *@param trade: the entire trade
	 */
	public boolean shouldReprice(Map<String, Object> trade){
		String taxonomyString = (String) trade.get(DtccValue.TAXONOMY.toString());
		Set<String> repricedTaxonomies = Taxonomy.stringValues(); 
		boolean shouldReprice = false;
		
		if (repricedTaxonomies.contains(taxonomyString)){
			Taxonomy taxonomy = Taxonomy.toEnum(taxonomyString);
			AbstractFilter filter = getFilter(taxonomy);
			shouldReprice = tradeMeetsCriteria(filter, trade);
		}
		return shouldReprice;
	}

	abstract protected boolean tradeMeetsCriteria(AbstractFilter filter, Map<String, Object> trade); 
	
	/**
	 * @return the proper filter for the Taxonomy
	 */
	public static AbstractFilter getFilter(Taxonomy taxonomy){
		switch(taxonomy){
		case INTERESTRATE_IRSWAP_FIXEDFLOAT:
			return new IRSwapFixedFloatFilter();
		case INTERESTRATE_IRSWAP_OIS:
			return new IRSwapOISFilter();
		case INTERESTRATE_IRSWAP_BASIS:
			return new IRSwapBasisFilter();
		case INTERESTRATE_CROSSCURRENCY_BASIS:
			return new CrossCurrencyBasisFilter();
		case INTERESTRATE_CROSSCURRENCY_FIXEDFLOAT:
			return new CrossCurrencyFixedFloatFilter();
		case INTERESTRATE_CROSSCURRENCY_FIXEDFIXED:
			return new CrossCurrencyFixedFixedFilter();
		case INTERESTRATE_OPTION_SWAPTION:
			return new OptionSwaptionFilter();
		case INTERESTRATE_CAPFLOOR:
			return new CapFloorFilter();	
		default:
			System.out.println("AbstractFilter: was passed a bad Taxonomy");
			return null;
		}
	}
}
