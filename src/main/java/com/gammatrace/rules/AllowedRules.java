/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.rules;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;

/**
 * Assumption: String columns cannot be null
 * The allowed values for String columns
 */
public class AllowedRules extends AbstractRules{
	private final static Map <String, Set <String>> allowedValues; // the values that are allowed
	static {		
		Map<String, Set <String>> allowedMap = new HashMap <String, Set <String>> ();
		insertRule(DtccValue.ACTION, Arrays.asList("NEW", "CORRECT"), allowedMap);
		insertRule(DtccValue.CLEARED, Arrays.asList("C", "U", EMPTY_STRING), allowedMap);
		insertRule(DtccValue.INDICATION_OF_COLLATERALIZATION, Arrays.asList("FC", "PC", "OC", "UC", EMPTY_STRING), allowedMap);
		insertRule(DtccValue.INDICATION_OF_END_USER_EXCEPTION, Arrays.asList("N", "Y", EMPTY_STRING), allowedMap);
		insertRule(DtccValue.INDICATION_OF_OTHER_PRICE_AFFECTING_TERM, Arrays.asList("N"), allowedMap);
		insertRule(DtccValue.PRICE_FORMING_CONTINUATION_DATA, Arrays.asList("Trade", "Termination"), allowedMap);	
		insertRule(DtccValue.BLOCK_TRADES_AND_LARGE_NOTIONAL_OFF_FACILITY_SWAPS, Arrays.asList("N", "Y", EMPTY_STRING), allowedMap);
		insertRule(DtccValue.EXECUTION_VENUE, Arrays.asList("OFF", "ON", EMPTY_STRING), allowedMap);	
		insertRule(DtccValue.EMBEDED_OPTION, Arrays.asList(EMPTY_STRING), allowedMap);		
		insertRule(DtccValue.DAY_COUNT_CONVENTION, Arrays.asList("30/360", "ACT/360", "1/1", "ACT/365.FIXED", "ACT/ACT.ICMA", "ACT/ACT.ISDA", "ACT/ACT.ISMA", "ACT/nACT", "BD252", "BUS/252"), allowedMap);
		insertRule(DtccValue.DAY_COUNT_CONVENTION, Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT, ANY_VALUE, allowedMap);
		insertRule(DtccValue.SETTLEMENT_CURRENCY, currencies, allowedMap);
		insertRule(DtccValue.ASSET_CLASS, Arrays.asList("IR"), allowedMap);
		insertRule(DtccValue.SUB_ASSET_CLASS_FOR_OTHER_COMMODITY, BLANK_SET, allowedMap);
		
		insertRule(DtccValue.PRICE_NOTATION_TYPE, Arrays.asList("Percent", "BasisPoints"), allowedMap);
		insertRule(DtccValue.PRICE_NOTATION_TYPE, Taxonomy.INTERESTRATE_IRSWAP_BASIS, union(Arrays.asList("Percent", "BasisPoints"), BLANK_SET), allowedMap);
		insertRule(DtccValue.PRICE_NOTATION_TYPE, Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, union(Arrays.asList("Percent", "BasisPoints"), BLANK_SET), allowedMap);
		
		insertRule(DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, union(BLANK_SET, currencies), allowedMap);
		insertRule(DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, currencies, allowedMap);
		insertRule(DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, Taxonomy.INTERESTRATE_CAPFLOOR, BLANK_SET, allowedMap);
		insertRule(DtccValue.NOTIONAL_CURRENCY_1, currencies, allowedMap);
		insertRule(DtccValue.NOTIONAL_CURRENCY_2, currencies, allowedMap);
		insertRule(DtccValue.NOTIONAL_CURRENCY_2, Taxonomy.INTERESTRATE_CAPFLOOR, BLANK_SET, allowedMap);
		
		insertRule(DtccValue.PAYMENT_FREQUENCY_1, Arrays.asList("28D","1M","3M","6M","12M","1Y","1T"), allowedMap);
		insertRule(DtccValue.PAYMENT_FREQUENCY_2, Arrays.asList("28D","1M","3M","6M","12M","1Y","1T"), allowedMap);
		insertRule(DtccValue.PAYMENT_FREQUENCY_2, Taxonomy.INTERESTRATE_CAPFLOOR, BLANK_SET, allowedMap);
		
		insertRule(DtccValue.RESET_FREQUENCY_1, Arrays.asList("28D","1M","3M","6M","12M","1Y","1T"), allowedMap);
		insertRule(DtccValue.RESET_FREQUENCY_1, Taxonomy.INTERESTRATE_IRSWAP_BASIS, Arrays.asList("1D","28D","1M","3M","6M","12M","1Y","1T"), allowedMap);
		insertRule(DtccValue.RESET_FREQUENCY_1, Taxonomy.INTERESTRATE_IRSWAP_OIS, Arrays.asList("1D","28D","1M","3M","6M","12M","1Y","1T"), allowedMap);
		insertRule(DtccValue.RESET_FREQUENCY_1, Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, BLANK_SET, allowedMap);
		insertRule(DtccValue.RESET_FREQUENCY_2, Arrays.asList("28D","1M","3M","6M","12M","1Y","1T"), allowedMap);
		insertRule(DtccValue.RESET_FREQUENCY_2, Taxonomy.INTERESTRATE_IRSWAP_BASIS, Arrays.asList("1D","28D","1M","3M","6M","12M","1Y","1T"), allowedMap);
		insertRule(DtccValue.RESET_FREQUENCY_2, Taxonomy.INTERESTRATE_IRSWAP_OIS, Arrays.asList("1D","28D","1M","3M","6M","12M","1Y","1T"), allowedMap);
		insertRule(DtccValue.RESET_FREQUENCY_2, Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, BLANK_SET, allowedMap);
		insertRule(DtccValue.RESET_FREQUENCY_2, Taxonomy.INTERESTRATE_CAPFLOOR, BLANK_SET, allowedMap);
		
		insertRule(DtccValue.OPTION_TYPE, BLANK_SET, allowedMap);
		insertRule(DtccValue.OPTION_TYPE, Taxonomy.INTERESTRATE_OPTION_SWAPTION, Arrays.asList("D-", "PF", "RF"), allowedMap);
		insertRule(DtccValue.OPTION_TYPE, Taxonomy.INTERESTRATE_CAPFLOOR, Arrays.asList("CF", "PC", "F-"), allowedMap);
		insertRule(DtccValue.OPTION_FAMILY, BLANK_SET, allowedMap);
		insertRule(DtccValue.OPTION_FAMILY, Taxonomy.INTERESTRATE_OPTION_SWAPTION, Arrays.asList("EU"), allowedMap);
		insertRule(DtccValue.OPTION_CURRENCY, BLANK_SET, allowedMap);
		insertRule(DtccValue.OPTION_CURRENCY, Taxonomy.INTERESTRATE_OPTION_SWAPTION, union(BLANK_SET, currencies), allowedMap);
		insertRule(DtccValue.OPTION_CURRENCY, Taxonomy.INTERESTRATE_CAPFLOOR, currencies, allowedMap);
		
		insertRule(DtccValue.OPTION_LOCK_PERIOD, BLANK_SET, allowedMap);
		insertRule(DtccValue.PRICE_NOTATION2_TYPE, union(BLANK_SET, Arrays.asList("Percent","BasisPoints","Spread")), allowedMap);
		insertRule(DtccValue.PRICE_NOTATION2_TYPE, Taxonomy.INTERESTRATE_CAPFLOOR, BLANK_SET, allowedMap);
		insertRule(DtccValue.PRICE_NOTATION3_TYPE, BLANK_SET, allowedMap);
		insertRule(DtccValue.PRICE_NOTATION3_TYPE, Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, union(BLANK_SET, currencies), allowedMap);
		
		for (Taxonomy t : Taxonomy.values()){
			insertRule(DtccValue.TAXONOMY, t, Arrays.asList(t.toString()), allowedMap);
		}
		
        allowedValues = Collections.unmodifiableMap(allowedMap);
	}
	
	/**
	 * @return true if value is allowed
	 */
	public static boolean containsAllowedValues(Taxonomy taxonomy, DtccValue dtccValue, String value){
		return hierarchyCheck(taxonomy, dtccValue, value, allowedValues, true);
	}
}
