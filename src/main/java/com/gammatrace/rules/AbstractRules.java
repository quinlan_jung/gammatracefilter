/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.rules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gammatrace.commonutils.Constants;
import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;

/**
 * The framework for all Rules
 */
public class AbstractRules {
	final static public String EMPTY_STRING = Constants.EMPTY_STRING;
	final static String ALL_TAXONOMY = "*";
	final static List <String> BLANK_SET = Arrays.asList(EMPTY_STRING);
	final static List<String> ANY_VALUE = null;
	final static String NULL = "null";
	final static String NOT_NULL = "not_null";
	final static public List <String> currencies = new ArrayList <String> ();
	
	static {
		// initialize valid currencies in accordance with ISO 4217
		for (Currency c : Currency.getAvailableCurrencies()) {
			currencies.add(c.getCurrencyCode());
		}
	}
	
	/**
	 * @return a new set, of union a + b
	 */
	static List<String> union (List<String> a, List<String> b){
		List <String> c = new ArrayList<String> (a);
		c.addAll(b);
		return c;
	}
	
	/**
	 * Insert a specific rule
	 * @param value: non null String
	 */
	static void insertRule(DtccValue dtccValue, Taxonomy taxonomy, String value, Map<String, String> map){
		String key = getTaxonomyKey(dtccValue, taxonomy);
		map.put(key, value);
	}
	
	/**
	 * Insert a general rule
	 * @param value: non null String
	 */
    static void insertRule(DtccValue dtccValue, String value, Map<String, String> map){
    	String key = getGeneralKey(dtccValue);
    	map.put(key, value);
	}
	
	/**
	 * Insert a specific rule
	 * @param values: if null, represents ANY_VALUE
	 */
	static void insertRule (DtccValue dtccValue, Taxonomy taxonomy, List<String> values, Map<String, Set<String>> map){
		String key = getTaxonomyKey(dtccValue, taxonomy);
		if (isNull(values)){
			map.put(key, null);
		}
		else{
			map.put(key, new HashSet<String> (values));
		}
	}
	
	/**
	 * Insert a general rule
	 * @param values: if null, represents ANY_VALUE
	 */
	static void insertRule(DtccValue dtccValue, List<String> values, Map<String, Set<String>> map){
		String key = getGeneralKey(dtccValue);
		if (isNull(values)){
			map.put(key, null);
		}
		else{
			map.put(key, new HashSet<String> (values));
		}
	}
	
	/**
	 * Get the key specific to a DtccValue and Taxonomy
	 */
	static public String getTaxonomyKey(DtccValue dtccValue, Taxonomy taxonomy){
		return String.format("%s-%s", taxonomy, dtccValue.toString());
	}
	
	/**
	 * Get the key specific to a DtccValue
	 */
	static public String getGeneralKey(DtccValue dtccValue){
		return String.format("%s-%s", ALL_TAXONOMY, dtccValue.toString());
	}
	
	/**
	 * If the specific rule exists, apply it
	 * Else if the general rule exists, apply it
	 * Else return defaultBoolean
	 */
	static boolean hierarchyCheck(Taxonomy taxonomy, DtccValue dtccValue, String value, Map<String, Set<String>> map, boolean defaultBoolean) {
		// Check the Taxonomy specific criteria if it exists
		String key;
		Set<String> values;
		
		key = getTaxonomyKey(dtccValue, taxonomy);
		if (map.containsKey(key)) {
			values = map.get(key);
			return values == ANY_VALUE || values.contains(value);
		}

		// Check the general criteria if it exists
		key = getGeneralKey(dtccValue);
		if (map.containsKey(key)) {
			values = map.get(key);
			return values == ANY_VALUE || values.contains(value);
		}

		// No rule
		return defaultBoolean;
	}
	
	static boolean isNull(Object value){
		return value == null;
	}
}
