/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.rules;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;

/**
 * Assumption: String columns cannot be null
 * The illegal values for String columns
 */
public class IllegalRules extends AbstractRules{

	private final static Map <String, Set <String>> illegalValues; // the values that are illegal
	
	static {        
        Map<String, Set <String>> illegalMap = new HashMap <String, Set <String>> ();
        insertRule(DtccValue.DAY_COUNT_CONVENTION, Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT, Arrays.asList(EMPTY_STRING), illegalMap);
        illegalValues = Collections.unmodifiableMap(illegalMap);
	}
	
	/**
	 * The illegal values for String columns
	 */
	public static boolean containsIllegalValues(Taxonomy taxonomy, DtccValue dtccValue, String value) {
		return hierarchyCheck(taxonomy, dtccValue, value, illegalValues, false);
	}
}
