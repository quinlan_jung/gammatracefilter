/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.rules;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;

/**
 * Assumption: non String columns are well formed or null
 * Determines whether a non String column should be null or not null
 */
public class NullRules extends AbstractRules{
	protected final static Map <String, String> nullCheckValues; // the values that can or cannot be null
	static {        
        // Add all the dtccValues that must not be null. No String that comes in is null.
        Map<String, String> nullCheckMap = new HashMap <String, String> ();
        insertRule(DtccValue.DISSEMINATION_ID, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.EXECUTION_TIMESTAMP, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.EFFECTIVE_DATE, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.END_DATE, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.PRICE_NOTATION, Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.PRICE_NOTATION, Taxonomy.INTERESTRATE_IRSWAP_OIS, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.PRICE_NOTATION, Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.PRICE_NOTATION, Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFLOAT, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.PRICE_NOTATION, Taxonomy.INTERESTRATE_OPTION_SWAPTION, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.PRICE_NOTATION, Taxonomy.INTERESTRATE_CAPFLOOR, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.ADDITIONAL_PRICE_NOTATION, Taxonomy.INTERESTRATE_CAPFLOOR, NULL, nullCheckMap);
        insertRule(DtccValue.ROUNDED_NOTIONAL_AMOUNT_1, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.OPTION_STRIKE_PRICE, NULL, nullCheckMap);
        insertRule(DtccValue.OPTION_PREMIUM, NULL, nullCheckMap);
        insertRule(DtccValue.OPTION_PREMIUM, Taxonomy.INTERESTRATE_CAPFLOOR, NOT_NULL, nullCheckMap);
        insertRule(DtccValue.PRICE_NOTATION2, Taxonomy.INTERESTRATE_CAPFLOOR, NULL, nullCheckMap);
        insertRule(DtccValue.PRICE_NOTATION3, NULL, nullCheckMap);

        nullCheckValues = Collections.unmodifiableMap(nullCheckMap);
	}
	
	/**
	 * @return true if the non String column passes null checks
	 */
	public static boolean passesNullCheck(Taxonomy taxonomy, DtccValue dtccValue, Object value){
		String key;
		String nullValue;
		
		key = getTaxonomyKey(dtccValue, taxonomy);
		if (nullCheckValues.containsKey(key)){
			nullValue = nullCheckValues.get(key);
			return nullValue.equals(NULL) ? isNull(value) : !isNull(value);
		}
		
		key = getGeneralKey(dtccValue);
		if (nullCheckValues.containsKey(key)){
			nullValue = nullCheckValues.get(key);
			return nullValue.equals(NULL) ? isNull(value) : !isNull(value);
		}
		return true;
	}
}
