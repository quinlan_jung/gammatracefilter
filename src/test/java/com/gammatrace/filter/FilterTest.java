/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filter;

import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;
import com.gammatrace.filtermanager.AbstractFilterManager;

public class FilterTest {

	@Before
	public void setUp() {
		FilterFramework.init();
	}

	/**
	 * Our filtering tests for DtccValues
	 */
	@Test
	public void filterTest() {
		DtccValue[] dtccValues = DtccValue.PULLED_COLUMN_ARRAY;
		for (Taxonomy taxonomy : Taxonomy.values()) {
			System.out.println("Taxonomy: " + taxonomy);
			AbstractFilter filter = AbstractFilterManager.getFilter(taxonomy);
			for (DtccValue dtccValue : dtccValues) {
				Set<TradeCheck> passChecks = FilterFramework.get(taxonomy, dtccValue, FilterFramework.passChecks);
				Set<TradeCheck> failChecks = FilterFramework.get(taxonomy, dtccValue, FilterFramework.failChecks);

				System.out.println(String.format("%s : %s : %s", dtccValue, "passChecks", printChecks(passChecks)));
				runPassChecks(passChecks, filter, dtccValue);
				System.out.println(String.format("%s : %s : %s", dtccValue, "failChecks", printChecks(failChecks)));
				runFailChecks(failChecks, filter, dtccValue);
			}
		}
	}

	/**
	 * Print out the values that we ran on a particular DtccValue and Taxonomy
	 */
	String printChecks(Set<TradeCheck> checks) {
		if (checks == null) {
			return "No test available";
		}

		String result = "";
		String delimiter = "";
		for (TradeCheck check : checks) {
			result += delimiter;

			Object value = check.getValue();
			if (value instanceof String && value.equals(FilterFramework.EMPTY_STRING)) {
				result += "EMPTY_STRING";
			} else {
				result += value;
			}
			delimiter = ",";
		}
		return result;
	}

	/**
	 * Run the checks that we expect to be True
	 */
	void runPassChecks(Set<TradeCheck> checks, AbstractFilter filter, DtccValue dtccValue) {
		if (checks == null) {
			return;
		}

		for (TradeCheck tradeCheck : checks) {
			boolean result = filter.dtccValueMeetsCriteria(dtccValue, tradeCheck.getValue(), tradeCheck.getTrade());
			Assert.assertTrue("Failed: " +  dtccValue, result);
		}
	}

	/**
	 * Run the checks we expect to be False
	 */
	void runFailChecks(Set<TradeCheck> checks, AbstractFilter filter, DtccValue dtccValue) {
		if (checks == null) {
			return;
		}

		for (TradeCheck tradeCheck : checks) {
			boolean result = filter.dtccValueMeetsCriteria(dtccValue, tradeCheck.getValue(), tradeCheck.getTrade());
			Assert.assertFalse("Failed: " +  dtccValue, result);
		}
	}

}
