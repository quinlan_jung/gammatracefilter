/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filter;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;
import com.gammatrace.rules.AbstractRules;

/**
 * Filter testing framework class
 *
 */
public class AbstractFilterFramework {

	/**
	 * The maps that contains values to test on filters of different instruments
	 * We expect the passChecks to yield true when we filter, and failChecks to yield false
	 */
	static public final Map<String, Set<TradeCheck>> passChecks = new HashMap <String, Set<TradeCheck>> ();
	static public final Map<String, Set<TradeCheck>> failChecks = new HashMap <String, Set<TradeCheck>> ();

	/**
	 * Get the tradeChecks for a particular taxonomy. If the specific tradecheck isnt there, the general one is used.
	 * @return Set<TradeCheck> if checks exist, else return null
	 */
	static Set<TradeCheck> get (Taxonomy taxonomy, DtccValue dtccValue, Map<String, Set<TradeCheck>> map){
		String key;
		
		key = AbstractRules.getTaxonomyKey(dtccValue, taxonomy);
		if (map.containsKey(key)) {
			return map.get(key);
		}

		// Check the general criteria if it exists
		key = AbstractRules.getGeneralKey(dtccValue);
		if (map.containsKey(key)) {
			return map.get(key);
		}
		
		return null;
	}

	/**
	 * Putting checks into a check map
	 * Specific check, generic trade, many values
	 */
	static void putList (Taxonomy taxonomy, DtccValue dtccValue, List<String> values, Map <String, Set<TradeCheck>> map){
		for (Object value : values){
			put(taxonomy, dtccValue, value, map);
		}
	}

	/**
	 * Putting checks into a check map
	 * Specific check, specific trade, many values
	 */
	static void putList (Taxonomy taxonomy, DtccValue dtccValue, List<String> values, Map<String, Object> trade, Map <String, Set<TradeCheck>> map){
		for (Object value : values){
			put(taxonomy, dtccValue, value, trade, map);
		}
	}

	/**
	 * Putting checks into a check map
	 * Generic check, generic trade, many values
	 */
	static void putList (DtccValue dtccValue, List<String> values, Map <String, Set<TradeCheck>> map){
		for (Object value : values){
			put(dtccValue, value, map);
		}
	}

	/**
	 * Putting checks into a check map
	 * Generic check, specific trade, many values
	 */
	static void putList (DtccValue dtccValue, List<String> values, Map<String, Object> trade, Map <String, Set<TradeCheck>> map){
		for (Object value : values){
			put(dtccValue, value, trade, map);
		}
	}

	/**
	 * Putting checks into a check map
	 * Specific check, specific trade, one value
	 */
	static void put (Taxonomy taxonomy, DtccValue dtccValue, Object value, Map<String, Object> trade, Map <String, Set<TradeCheck>> map){
		String key = AbstractRules.getTaxonomyKey(dtccValue, taxonomy);
		put (key, value, trade, map);
	}

	/**
	 * Putting checks into a check map
	 * Specific check, generic trade, one value
	 */
	static void put (Taxonomy taxonomy, DtccValue dtccValue, Object value, Map <String, Set<TradeCheck>> map){
		String key = AbstractRules.getTaxonomyKey(dtccValue, taxonomy);
		put (key, value, map);
	}

	/**
	 * Putting checks into a check map
	 * Generic check, specific trade, one value
	 */
	static void put (DtccValue dtccValue, Object value, Map<String, Object> trade, Map <String, Set<TradeCheck>> map){
		String key = AbstractRules.getGeneralKey(dtccValue);
		put (key, value, trade, map);
	}

	/**
	 * Putting checks into a check map
	 * Generic check, generic trade, one value
	 */
	static void put (DtccValue dtccValue, Object value, Map <String, Set<TradeCheck>> map){
		String key = AbstractRules.getGeneralKey(dtccValue);
		put (key, value, map);
	}
	
	/**
	 * Putting checks into a check map
	 * @param key: *-dtccValue or TAXONOMY-dtccValue
	 */
	static private void put (String key, Object value, Map<String, Object> trade, Map <String, Set<TradeCheck>> map){
		if (value instanceof Collection){
			throw new IllegalArgumentException("Use putList");
		}
		TradeCheck tradeCheck = new TradeCheck(value, trade);
		addEntry(key, tradeCheck, map);
	}
	
	/**
	 * Putting checks into a check map
	 * @param key: *-dtccValue or TAXONOMY-dtccValue
	 */
	static private void put (String key, Object value, Map <String, Set<TradeCheck>> map){
		if (value instanceof Collection){
			throw new IllegalArgumentException("Use putList");
		}
		TradeCheck tradeCheck = new TradeCheck(value);
		addEntry(key, tradeCheck, map);
	}
	
	/**
	 * Add an entry into a map
	 */
	static void addEntry(String dtccValueKey, TradeCheck tradeCheck, Map <String, Set<TradeCheck>> map){
		Set<TradeCheck> set;
		if (!map.containsKey(dtccValueKey)){
			set = new HashSet<TradeCheck> ();
			map.put(dtccValueKey, set);
		}
		else{
			set = map.get(dtccValueKey);
		}
		set.add(tradeCheck);
	}
}
