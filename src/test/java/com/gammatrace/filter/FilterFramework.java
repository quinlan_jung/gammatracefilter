/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Taxonomy;
import com.gammatrace.rules.AbstractRules;

/**
 * The class where we store all the checks
 */
public class FilterFramework extends AbstractFilterFramework{
	final static int INTEGER = 123;
	final static long LONG = 123l;
	final static double DOUBLE = 3.14d;
	final static String RANDOM_STRING = "RANDOM_STRING";
	final static String EMPTY_STRING = AbstractRules.EMPTY_STRING;
	final static List <String> currencies = AbstractRules.currencies;
	
	static{
		/**
		 * Trades we use for testing
		 */
		Map<String, Object> EFFECTIVE_DATE_TRADE = new HashMap<String, Object>();
		EFFECTIVE_DATE_TRADE.put(DtccValue.EFFECTIVE_DATE.toString(), 1425400992L);
		
		Map<String, Object> NOTIONAL_CURRENCY1AND2 = new HashMap<String, Object>();
		NOTIONAL_CURRENCY1AND2.put(DtccValue.NOTIONAL_CURRENCY_1.toString(), "USD");
		NOTIONAL_CURRENCY1AND2.put(DtccValue.NOTIONAL_CURRENCY_2.toString(), "CDN");
		
		Map<String, Object> BAD_NOTIONAL_CURRENCY1AND2 = new HashMap<String, Object>();
		BAD_NOTIONAL_CURRENCY1AND2.put(DtccValue.NOTIONAL_CURRENCY_1.toString(), "XYZ");
		BAD_NOTIONAL_CURRENCY1AND2.put(DtccValue.NOTIONAL_CURRENCY_2.toString(), "ABC");
		
		Map<String, Object> BLANK_ADDITIONAL_PRICE_NOTATION_TYPE = new HashMap<String, Object>();
		BLANK_ADDITIONAL_PRICE_NOTATION_TYPE.put(DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE.toString(), EMPTY_STRING);
		
		Map<String, Object> BLANK_ADDITIONAL_PRICE_NOTATION_TYPE_WITH_OPTION_PREMIUM = new HashMap<String, Object>();
		BLANK_ADDITIONAL_PRICE_NOTATION_TYPE_WITH_OPTION_PREMIUM.put(DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE.toString(), EMPTY_STRING);
		BLANK_ADDITIONAL_PRICE_NOTATION_TYPE_WITH_OPTION_PREMIUM.put(DtccValue.OPTION_PREMIUM.toString(), DOUBLE);

		Map<String, Object> ADDITIONAL_PRICE_NOTATION_TYPE = new HashMap<String, Object>();
		ADDITIONAL_PRICE_NOTATION_TYPE.put(DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE.toString(), RANDOM_STRING);
		
		Map<String, Object> ADDITIONAL_PRICE_NOTATION_TYPE_WITH_NULL_OPTION_PREMIUM = new HashMap<String, Object>();
		ADDITIONAL_PRICE_NOTATION_TYPE_WITH_NULL_OPTION_PREMIUM.put(DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE.toString(), RANDOM_STRING);
		ADDITIONAL_PRICE_NOTATION_TYPE_WITH_NULL_OPTION_PREMIUM.put(DtccValue.OPTION_PREMIUM.toString(), null);
		
		Map<String, Object> SETTLEMENT_CURRENCY = new HashMap<String, Object>();
		SETTLEMENT_CURRENCY.put(DtccValue.SETTLEMENT_CURRENCY.toString(), "USD");

		Map<String, Object> BAD_SETTLEMENT_CURRENCY = new HashMap<String, Object>();
		BAD_SETTLEMENT_CURRENCY.put(DtccValue.SETTLEMENT_CURRENCY.toString(), "XYZ");
		
		Map<String, Object> FIXED_UNDERLYING_ASSET_1 = new HashMap<String, Object>();
		FIXED_UNDERLYING_ASSET_1.put(DtccValue.UNDERLYING_ASSET_1.toString(), "FIXED");
		FIXED_UNDERLYING_ASSET_1.put(DtccValue.UNDERLYING_ASSET_2.toString(), "");
		
		Map<String, Object> FIXED_UNDERLYING_ASSET_2 = new HashMap<String, Object>();
		FIXED_UNDERLYING_ASSET_2.put(DtccValue.UNDERLYING_ASSET_2.toString(), "FIXED");
		FIXED_UNDERLYING_ASSET_2.put(DtccValue.UNDERLYING_ASSET_1.toString(), "");
		
		Map<String, Object> PAYMENT_FREQUENCY_1 = new HashMap<String, Object>();
		PAYMENT_FREQUENCY_1.put(DtccValue.PAYMENT_FREQUENCY_1.toString(), "28D");
		
		Map<String, Object> FLOATING_PAYMENT_FREQUENCY_1 = new HashMap<String, Object>();
		FLOATING_PAYMENT_FREQUENCY_1.put(DtccValue.PAYMENT_FREQUENCY_1.toString(), "28D");
		FLOATING_PAYMENT_FREQUENCY_1.put(DtccValue.UNDERLYING_ASSET_2.toString(), "FIXED");
		FLOATING_PAYMENT_FREQUENCY_1.put(DtccValue.UNDERLYING_ASSET_1.toString(), "");
		
		Map<String, Object> FIXED_PAYMENT_FREQUENCY_1 = new HashMap<String, Object>();
		FIXED_PAYMENT_FREQUENCY_1.put(DtccValue.PAYMENT_FREQUENCY_1.toString(), "28D");
		FIXED_PAYMENT_FREQUENCY_1.put(DtccValue.UNDERLYING_ASSET_1.toString(), "FIXED");
		FIXED_PAYMENT_FREQUENCY_1.put(DtccValue.UNDERLYING_ASSET_2.toString(), "");
		
		Map<String, Object> FLOATING_PAYMENT_FREQUENCY_2 = new HashMap<String, Object>();
		FLOATING_PAYMENT_FREQUENCY_2.put(DtccValue.PAYMENT_FREQUENCY_2.toString(), "28D");
		FLOATING_PAYMENT_FREQUENCY_2.put(DtccValue.UNDERLYING_ASSET_1.toString(), "FIXED");
		FLOATING_PAYMENT_FREQUENCY_2.put(DtccValue.UNDERLYING_ASSET_2.toString(), "");
		
		Map<String, Object> FIXED_PAYMENT_FREQUENCY_2 = new HashMap<String, Object>();
		FIXED_PAYMENT_FREQUENCY_2.put(DtccValue.PAYMENT_FREQUENCY_2.toString(), "28D");
		FIXED_PAYMENT_FREQUENCY_2.put(DtccValue.UNDERLYING_ASSET_2.toString(), "FIXED");
		FIXED_PAYMENT_FREQUENCY_2.put(DtccValue.UNDERLYING_ASSET_1.toString(), "");
		
		Map<String, Object> PRICE_NOTATION = new HashMap<String, Object>();
		PRICE_NOTATION.put(DtccValue.PRICE_NOTATION.toString(), DOUBLE);
		
		Map<String, Object> OPTION_CURRENCY = new HashMap<String, Object>();
		OPTION_CURRENCY.put(DtccValue.OPTION_CURRENCY.toString(), "USD");
		
		Map<String, Object> OPTION_CURRENCY_WITH_ADDITIONAL_PRICE_NOTATION = new HashMap<String, Object>();
		OPTION_CURRENCY_WITH_ADDITIONAL_PRICE_NOTATION.put(DtccValue.OPTION_CURRENCY.toString(), "USD");
		OPTION_CURRENCY_WITH_ADDITIONAL_PRICE_NOTATION.put(DtccValue.ADDITIONAL_PRICE_NOTATION.toString(), DOUBLE);
		
		Map<String, Object> BLANK_OPTION_CURRENCY = new HashMap<String, Object>();
		BLANK_OPTION_CURRENCY.put(DtccValue.OPTION_CURRENCY.toString(), EMPTY_STRING);
		
		Map<String, Object> PRICE_NOTATION2_TYPE = new HashMap<String, Object>();
		PRICE_NOTATION2_TYPE.put(DtccValue.PRICE_NOTATION2_TYPE.toString(), "Percent");
		
		Map<String, Object> BLANK_PRICE_NOTATION2_TYPE = new HashMap<String, Object>();
		BLANK_PRICE_NOTATION2_TYPE.put(DtccValue.PRICE_NOTATION2_TYPE.toString(), EMPTY_STRING);
		
		Map<String, Object> PRICE_NOTATION3_TYPE = new HashMap<String, Object>();
		PRICE_NOTATION3_TYPE.put(DtccValue.PRICE_NOTATION3_TYPE.toString(), "Percent");
		
		Map<String, Object> BLANK_PRICE_NOTATION3_TYPE = new HashMap<String, Object>();
		BLANK_PRICE_NOTATION3_TYPE.put(DtccValue.PRICE_NOTATION3_TYPE.toString(), EMPTY_STRING);
		
		/**
		 * All our checks for the DtccValues
		 */
		put(DtccValue.DISSEMINATION_ID, LONG, passChecks);
		put(DtccValue.DISSEMINATION_ID, null, failChecks);
		
		put(DtccValue.ORIGINAL_DISSEMINATION_ID, LONG, passChecks);
		put(DtccValue.ORIGINAL_DISSEMINATION_ID, null, passChecks);
		
		putList(DtccValue.ACTION, Arrays.asList("NEW", "CORRECT"), passChecks);
		put(DtccValue.ACTION, RANDOM_STRING, failChecks);
		
		put(DtccValue.EXECUTION_TIMESTAMP, LONG, passChecks);
		put(DtccValue.EXECUTION_TIMESTAMP, null, failChecks);
		
		putList(DtccValue.CLEARED, Arrays.asList("C", "U", EMPTY_STRING), passChecks);
		put(DtccValue.CLEARED, RANDOM_STRING, failChecks);
		
		putList(DtccValue.INDICATION_OF_COLLATERALIZATION, Arrays.asList("FC", "PC", "OC", "UC", EMPTY_STRING), passChecks);
		put(DtccValue.INDICATION_OF_COLLATERALIZATION, RANDOM_STRING, failChecks);
		
		putList(DtccValue.INDICATION_OF_END_USER_EXCEPTION, Arrays.asList("N", "Y", EMPTY_STRING), passChecks);
		put(DtccValue.INDICATION_OF_END_USER_EXCEPTION, RANDOM_STRING, failChecks);
		
		putList(DtccValue.INDICATION_OF_OTHER_PRICE_AFFECTING_TERM, Arrays.asList("N"), passChecks);
		put(DtccValue.INDICATION_OF_OTHER_PRICE_AFFECTING_TERM, RANDOM_STRING, failChecks);
		
		putList(DtccValue.BLOCK_TRADES_AND_LARGE_NOTIONAL_OFF_FACILITY_SWAPS, Arrays.asList("N", "Y", EMPTY_STRING), passChecks);
		put(DtccValue.BLOCK_TRADES_AND_LARGE_NOTIONAL_OFF_FACILITY_SWAPS, RANDOM_STRING, failChecks);
		
		putList(DtccValue.EXECUTION_VENUE, Arrays.asList("OFF", "ON", EMPTY_STRING), passChecks);
		put(DtccValue.EXECUTION_VENUE, RANDOM_STRING, failChecks);
		
		put(DtccValue.EFFECTIVE_DATE, LONG, passChecks);
		put(DtccValue.EFFECTIVE_DATE, null, failChecks);
		
		put(DtccValue.END_DATE, 1425400993L, EFFECTIVE_DATE_TRADE, passChecks);
		put(DtccValue.END_DATE, 1425400991L, EFFECTIVE_DATE_TRADE, failChecks);
		
		putList(DtccValue.DAY_COUNT_CONVENTION, Arrays.asList("30/360", "ACT/360", "1/1", "ACT/365.FIXED", "ACT/ACT.ICMA", "ACT/ACT.ISDA", "ACT/ACT.ISMA", "ACT/nACT", "BD252", "BUS/252"), passChecks);
		put(DtccValue.DAY_COUNT_CONVENTION, RANDOM_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT, DtccValue.DAY_COUNT_CONVENTION, RANDOM_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT, DtccValue.DAY_COUNT_CONVENTION, EMPTY_STRING, failChecks);
		
		putList(DtccValue.SETTLEMENT_CURRENCY, currencies, passChecks);
		put(DtccValue.SETTLEMENT_CURRENCY, RANDOM_STRING, failChecks);
		for (Taxonomy t : Arrays.asList(Taxonomy.INTERESTRATE_CAPFLOOR, Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFLOAT)){
			put(t, DtccValue.SETTLEMENT_CURRENCY, "USD", NOTIONAL_CURRENCY1AND2, passChecks);
			put(t, DtccValue.SETTLEMENT_CURRENCY, "JPY", NOTIONAL_CURRENCY1AND2, failChecks);
			put(t, DtccValue.SETTLEMENT_CURRENCY, "XYZ", BAD_NOTIONAL_CURRENCY1AND2, failChecks);
		}
	
		put(DtccValue.ASSET_CLASS, "IR", passChecks);
		put(DtccValue.ASSET_CLASS, RANDOM_STRING, failChecks);
		
		put(DtccValue.SUB_ASSET_CLASS_FOR_OTHER_COMMODITY, EMPTY_STRING, passChecks);
		put(DtccValue.SUB_ASSET_CLASS_FOR_OTHER_COMMODITY, RANDOM_STRING, failChecks);
		
		for (Taxonomy t : Taxonomy.values()){
			put(t, DtccValue.TAXONOMY, t.toString(), passChecks);
			putList(t, DtccValue.TAXONOMY, minus(t), failChecks);
		}
		
		putList(DtccValue.PRICE_FORMING_CONTINUATION_DATA, Arrays.asList("Trade", "Termination"), passChecks);
		put(DtccValue.PRICE_FORMING_CONTINUATION_DATA, RANDOM_STRING, failChecks);
		
		putList(DtccValue.PRICE_NOTATION_TYPE, Arrays.asList("Percent", "BasisPoints"), passChecks);
		put(DtccValue.PRICE_NOTATION_TYPE, RANDOM_STRING, failChecks);
		put(DtccValue.PRICE_NOTATION_TYPE, EMPTY_STRING, failChecks);
		
		putList(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.PRICE_NOTATION_TYPE, Arrays.asList("Percent", "BasisPoints"), passChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.PRICE_NOTATION_TYPE, RANDOM_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.PRICE_NOTATION_TYPE, EMPTY_STRING, PRICE_NOTATION2_TYPE, passChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.PRICE_NOTATION_TYPE, EMPTY_STRING, BLANK_PRICE_NOTATION2_TYPE, failChecks);
		
		putList(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.PRICE_NOTATION_TYPE, Arrays.asList("Percent", "BasisPoints"), passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.PRICE_NOTATION_TYPE, RANDOM_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.PRICE_NOTATION_TYPE, EMPTY_STRING, PRICE_NOTATION2_TYPE, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.PRICE_NOTATION_TYPE, EMPTY_STRING, BLANK_PRICE_NOTATION2_TYPE, failChecks);
		
		put(DtccValue.PRICE_NOTATION, DOUBLE, passChecks);
		put(DtccValue.PRICE_NOTATION, null, failChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.PRICE_NOTATION, DOUBLE, passChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.PRICE_NOTATION, null, PRICE_NOTATION2_TYPE, passChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.PRICE_NOTATION, null, BLANK_PRICE_NOTATION2_TYPE, failChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.PRICE_NOTATION, DOUBLE, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.PRICE_NOTATION, null, PRICE_NOTATION2_TYPE, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.PRICE_NOTATION, null, BLANK_PRICE_NOTATION2_TYPE, failChecks);	
		
		putList(Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, currencies, passChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, EMPTY_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, RANDOM_STRING, failChecks);
		
		put(Taxonomy.INTERESTRATE_IRSWAP_OIS, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, "USD", SETTLEMENT_CURRENCY, passChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_OIS, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, EMPTY_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_OIS, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, RANDOM_STRING, failChecks);
		putList(Taxonomy.INTERESTRATE_IRSWAP_OIS, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, currencies, BAD_SETTLEMENT_CURRENCY, failChecks);
		
		putList(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, currencies, passChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, EMPTY_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, RANDOM_STRING, failChecks);
		
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, "USD", SETTLEMENT_CURRENCY, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, EMPTY_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, RANDOM_STRING, failChecks);
		putList(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, currencies, BAD_SETTLEMENT_CURRENCY, failChecks);

		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFLOAT, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, "USD", SETTLEMENT_CURRENCY, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFLOAT, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, EMPTY_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFLOAT, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, RANDOM_STRING, failChecks);
		putList(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFLOAT, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, currencies, BAD_SETTLEMENT_CURRENCY, failChecks);
		
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, "USD", NOTIONAL_CURRENCY1AND2, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, EMPTY_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, RANDOM_STRING, failChecks);
		putList(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, currencies, BAD_NOTIONAL_CURRENCY1AND2, failChecks);
		
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, EMPTY_STRING, passChecks);
		putList(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.ADDITIONAL_PRICE_NOTATION_TYPE, currencies, failChecks);
		
		put(DtccValue.ADDITIONAL_PRICE_NOTATION, null, BLANK_ADDITIONAL_PRICE_NOTATION_TYPE, passChecks);
		put(DtccValue.ADDITIONAL_PRICE_NOTATION, DOUBLE, ADDITIONAL_PRICE_NOTATION_TYPE, passChecks);
		put(DtccValue.ADDITIONAL_PRICE_NOTATION, DOUBLE, BLANK_ADDITIONAL_PRICE_NOTATION_TYPE, failChecks);
		put(DtccValue.ADDITIONAL_PRICE_NOTATION, null, ADDITIONAL_PRICE_NOTATION_TYPE, failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.ADDITIONAL_PRICE_NOTATION, null, passChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.ADDITIONAL_PRICE_NOTATION, DOUBLE,failChecks);
		
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.ADDITIONAL_PRICE_NOTATION, DOUBLE, ADDITIONAL_PRICE_NOTATION_TYPE_WITH_NULL_OPTION_PREMIUM, passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.ADDITIONAL_PRICE_NOTATION, DOUBLE, BLANK_ADDITIONAL_PRICE_NOTATION_TYPE_WITH_OPTION_PREMIUM, failChecks);
		
		putList(DtccValue.NOTIONAL_CURRENCY_1, currencies, passChecks);
		put(DtccValue.NOTIONAL_CURRENCY_1, RANDOM_STRING, failChecks);
		for (Taxonomy t : Arrays.asList(Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT, Taxonomy.INTERESTRATE_IRSWAP_BASIS, Taxonomy.INTERESTRATE_IRSWAP_OIS, Taxonomy.INTERESTRATE_OPTION_SWAPTION, Taxonomy.INTERESTRATE_CAPFLOOR)){
			put(t, DtccValue.NOTIONAL_CURRENCY_1, "USD", SETTLEMENT_CURRENCY, passChecks);
			put(t, DtccValue.NOTIONAL_CURRENCY_1, "CDN", SETTLEMENT_CURRENCY, failChecks);
			put(t, DtccValue.NOTIONAL_CURRENCY_1, "XYZ", BAD_SETTLEMENT_CURRENCY, failChecks);
		}
		
		putList(DtccValue.NOTIONAL_CURRENCY_2, currencies, passChecks);
		put(DtccValue.NOTIONAL_CURRENCY_2, RANDOM_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.NOTIONAL_CURRENCY_2, EMPTY_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.NOTIONAL_CURRENCY_2, "USD", failChecks);
		for (Taxonomy t : Arrays.asList(Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT, Taxonomy.INTERESTRATE_IRSWAP_BASIS, Taxonomy.INTERESTRATE_IRSWAP_OIS, Taxonomy.INTERESTRATE_OPTION_SWAPTION)){
			put(t, DtccValue.NOTIONAL_CURRENCY_2, "USD", SETTLEMENT_CURRENCY, passChecks);
			put(t, DtccValue.NOTIONAL_CURRENCY_2, "CDN", SETTLEMENT_CURRENCY, failChecks);
			put(t, DtccValue.NOTIONAL_CURRENCY_2, "XYZ", BAD_SETTLEMENT_CURRENCY, failChecks);
		}

		put(DtccValue.ROUNDED_NOTIONAL_AMOUNT_1, LONG, passChecks);
		put(DtccValue.ROUNDED_NOTIONAL_AMOUNT_1, null, failChecks);
		
		putList(DtccValue.PAYMENT_FREQUENCY_1, Arrays.asList("28D","1M","3M","6M","12M","1Y","1T"), passChecks);
		put(DtccValue.PAYMENT_FREQUENCY_1, RANDOM_STRING, failChecks);
		putList(DtccValue.PAYMENT_FREQUENCY_2, Arrays.asList("28D","1M","3M","6M","12M","1Y","1T"), passChecks);
		put(DtccValue.PAYMENT_FREQUENCY_2, RANDOM_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.PAYMENT_FREQUENCY_2, EMPTY_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.PAYMENT_FREQUENCY_2, "28D", failChecks);
		
		putList(DtccValue.RESET_FREQUENCY_1, Arrays.asList("28D","1M","3M","6M","12M","1Y","1T"), FIXED_UNDERLYING_ASSET_2, passChecks);
		put(DtccValue.RESET_FREQUENCY_1, EMPTY_STRING, FIXED_UNDERLYING_ASSET_1, passChecks);
		putList(DtccValue.RESET_FREQUENCY_1, Arrays.asList("1D","28D","1M","3M","6M","12M","1Y","1T"), FIXED_UNDERLYING_ASSET_1, failChecks);
		put(DtccValue.RESET_FREQUENCY_1, EMPTY_STRING, FIXED_UNDERLYING_ASSET_2, failChecks);
		
		putList(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.RESET_FREQUENCY_1, Arrays.asList("1D","28D","1M","3M","6M","12M","1Y","1T"), FIXED_UNDERLYING_ASSET_2, passChecks);
		putList(Taxonomy.INTERESTRATE_IRSWAP_OIS, DtccValue.RESET_FREQUENCY_1, Arrays.asList("1D","28D","1M","3M","6M","12M","1Y","1T"), FIXED_UNDERLYING_ASSET_2, passChecks);
		putList(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.RESET_FREQUENCY_1, Arrays.asList("28D","1M","3M","6M","12M","1Y","1T"), passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.RESET_FREQUENCY_1, RANDOM_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.RESET_FREQUENCY_1, EMPTY_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.RESET_FREQUENCY_1, "28D", failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.RESET_FREQUENCY_1, "28D", PAYMENT_FREQUENCY_1, passChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.RESET_FREQUENCY_1, "1M", PAYMENT_FREQUENCY_1, failChecks);
		
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.RESET_FREQUENCY_1, "28D", FLOATING_PAYMENT_FREQUENCY_1, passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.RESET_FREQUENCY_1, EMPTY_STRING, FIXED_PAYMENT_FREQUENCY_1, passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.RESET_FREQUENCY_1, "1M", FLOATING_PAYMENT_FREQUENCY_1, failChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.RESET_FREQUENCY_1, EMPTY_STRING, FLOATING_PAYMENT_FREQUENCY_1, failChecks);
		
		putList(DtccValue.RESET_FREQUENCY_2, Arrays.asList("28D","1M","3M","6M","12M","1Y","1T"), FIXED_UNDERLYING_ASSET_1, passChecks);
		put(DtccValue.RESET_FREQUENCY_2, EMPTY_STRING, FIXED_UNDERLYING_ASSET_2, passChecks);
		putList(DtccValue.RESET_FREQUENCY_2, Arrays.asList("1D","28D","1M","3M","6M","12M","1Y","1T"), FIXED_UNDERLYING_ASSET_2, failChecks);
		put(DtccValue.RESET_FREQUENCY_2, EMPTY_STRING, FIXED_UNDERLYING_ASSET_1, failChecks);
		
		putList(Taxonomy.INTERESTRATE_IRSWAP_BASIS, DtccValue.RESET_FREQUENCY_2, Arrays.asList("1D","28D","1M","3M","6M","12M","1Y","1T"), FIXED_UNDERLYING_ASSET_1, passChecks);
		putList(Taxonomy.INTERESTRATE_IRSWAP_OIS, DtccValue.RESET_FREQUENCY_2, Arrays.asList("1D","28D","1M","3M","6M","12M","1Y","1T"), FIXED_UNDERLYING_ASSET_1, passChecks);
		putList(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.RESET_FREQUENCY_2, Arrays.asList("28D","1M","3M","6M","12M","1Y","1T"), passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS, DtccValue.RESET_FREQUENCY_2, RANDOM_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.RESET_FREQUENCY_2, EMPTY_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.RESET_FREQUENCY_2, "28D", failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.RESET_FREQUENCY_2, EMPTY_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.RESET_FREQUENCY_2, "28D", failChecks);
		
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.RESET_FREQUENCY_2, "28D", FLOATING_PAYMENT_FREQUENCY_2, passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.RESET_FREQUENCY_2, EMPTY_STRING, FIXED_PAYMENT_FREQUENCY_2, passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.RESET_FREQUENCY_2, "1M", FLOATING_PAYMENT_FREQUENCY_2, failChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.RESET_FREQUENCY_2, EMPTY_STRING, FLOATING_PAYMENT_FREQUENCY_2, failChecks);
		
		put(DtccValue.EMBEDED_OPTION, EMPTY_STRING, passChecks);
		put(DtccValue.EMBEDED_OPTION, RANDOM_STRING, failChecks);
		
		put(DtccValue.OPTION_STRIKE_PRICE, null, passChecks);
		put(DtccValue.OPTION_STRIKE_PRICE, DOUBLE, failChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_STRIKE_PRICE, DOUBLE, PRICE_NOTATION, passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_STRIKE_PRICE, DOUBLE + 1, PRICE_NOTATION, failChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_STRIKE_PRICE, null, PRICE_NOTATION, failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.OPTION_STRIKE_PRICE, DOUBLE/100, PRICE_NOTATION, passChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.OPTION_STRIKE_PRICE, DOUBLE, PRICE_NOTATION, failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.OPTION_STRIKE_PRICE, null, PRICE_NOTATION, failChecks);
		
		put(DtccValue.OPTION_TYPE, EMPTY_STRING, passChecks);
		put(DtccValue.OPTION_TYPE, RANDOM_STRING, failChecks);
		putList(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_TYPE, Arrays.asList("D-", "PF", "RF"), passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_TYPE, RANDOM_STRING, failChecks);
		putList(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.OPTION_TYPE, Arrays.asList("CF", "PC", "F-"), passChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.OPTION_TYPE, RANDOM_STRING, failChecks);
		
		put(DtccValue.OPTION_FAMILY, EMPTY_STRING, passChecks);
		put(DtccValue.OPTION_FAMILY, RANDOM_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_FAMILY, "EU", passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_FAMILY, RANDOM_STRING, failChecks);
		
		put(DtccValue.OPTION_CURRENCY, EMPTY_STRING, passChecks);
		put(DtccValue.OPTION_CURRENCY, RANDOM_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_CURRENCY, "USD", SETTLEMENT_CURRENCY, passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_CURRENCY, EMPTY_STRING, SETTLEMENT_CURRENCY, passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_CURRENCY, "JPY", SETTLEMENT_CURRENCY, failChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_CURRENCY, "XYZ", BAD_SETTLEMENT_CURRENCY, failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.OPTION_CURRENCY, "USD", SETTLEMENT_CURRENCY, passChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.OPTION_CURRENCY, "JPY", SETTLEMENT_CURRENCY, failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.OPTION_CURRENCY, "XYZ", BAD_SETTLEMENT_CURRENCY, failChecks);
		
		put(DtccValue.OPTION_PREMIUM, null, passChecks);
		put(DtccValue.OPTION_PREMIUM, DOUBLE, failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.OPTION_PREMIUM, DOUBLE, passChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.OPTION_PREMIUM, null, failChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_PREMIUM, DOUBLE, OPTION_CURRENCY, passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_PREMIUM, null, BLANK_OPTION_CURRENCY, passChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_PREMIUM, DOUBLE, OPTION_CURRENCY_WITH_ADDITIONAL_PRICE_NOTATION, failChecks);
		put(Taxonomy.INTERESTRATE_OPTION_SWAPTION, DtccValue.OPTION_PREMIUM, DOUBLE, BLANK_OPTION_CURRENCY, failChecks);
		
		put(DtccValue.OPTION_LOCK_PERIOD, EMPTY_STRING, passChecks);
		put(DtccValue.OPTION_LOCK_PERIOD, RANDOM_STRING, failChecks);
		
		putList(DtccValue.PRICE_NOTATION2_TYPE, Arrays.asList(EMPTY_STRING, "Percent","BasisPoints","Spread"), passChecks);
		put(DtccValue.PRICE_NOTATION2_TYPE, RANDOM_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.PRICE_NOTATION2_TYPE, EMPTY_STRING, passChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.PRICE_NOTATION2_TYPE, "Percent", failChecks);

		put(DtccValue.PRICE_NOTATION2, null, BLANK_PRICE_NOTATION2_TYPE, passChecks);
		put(DtccValue.PRICE_NOTATION2, DOUBLE, PRICE_NOTATION2_TYPE, passChecks);
		put(DtccValue.PRICE_NOTATION2, null, PRICE_NOTATION2_TYPE, failChecks);
		put(DtccValue.PRICE_NOTATION2, DOUBLE, BLANK_PRICE_NOTATION2_TYPE, failChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.PRICE_NOTATION2, null, PRICE_NOTATION2_TYPE, passChecks);
		put(Taxonomy.INTERESTRATE_CAPFLOOR, DtccValue.PRICE_NOTATION2, DOUBLE, PRICE_NOTATION2_TYPE, failChecks);
		
		put(DtccValue.PRICE_NOTATION3_TYPE, EMPTY_STRING, passChecks);
		put(DtccValue.PRICE_NOTATION3_TYPE, RANDOM_STRING, failChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.PRICE_NOTATION3_TYPE, EMPTY_STRING, NOTIONAL_CURRENCY1AND2, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.PRICE_NOTATION3_TYPE, "USD", NOTIONAL_CURRENCY1AND2, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.PRICE_NOTATION3_TYPE, "JPY", NOTIONAL_CURRENCY1AND2, failChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.PRICE_NOTATION3_TYPE, "XYZ", BAD_NOTIONAL_CURRENCY1AND2, failChecks);
		
		put(DtccValue.PRICE_NOTATION3, null, passChecks);
		put(DtccValue.PRICE_NOTATION3, DOUBLE, failChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.PRICE_NOTATION3, null, BLANK_PRICE_NOTATION3_TYPE, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.PRICE_NOTATION3, DOUBLE, PRICE_NOTATION3_TYPE, passChecks);
		put(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED, DtccValue.PRICE_NOTATION3, null, PRICE_NOTATION3_TYPE, failChecks);
	}
	

	/**
	 * @return All the taxonomies, except tminus
	 */
	static List<String> minus (Taxonomy tminus){
		Taxonomy [] taxonomies = Taxonomy.values();
		List<String> minusSet = new ArrayList<String>();
		for (int i = 0 ; i < taxonomies.length; i++){
			Taxonomy t = taxonomies [i];
			if (!t.equals(tminus)){
				minusSet.add(t.toString());
			}
		}
		return minusSet;
	}
	
	/**
	 * Call in @Before to initialize the static blocks
	 */
	static void init(){}
}
