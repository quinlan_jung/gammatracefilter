/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.filter;

import java.util.HashMap;
import java.util.Map;

/**
 * A check that we pass through a Filter, expecting it to return true or false
 * @param value: The value that we pass into the filter
 * @param trade: The trade that we pass into the filter
 */
public class TradeCheck {
	Object value;
	Map <String, Object> trade;
	
	public TradeCheck(Object value, Map<String, Object> trade){
		this.value = value;
		this.trade = trade;
	}

	public TradeCheck (Object value){
		this.value = value;
		this.trade = new HashMap<String, Object> ();
	}
	
	public Object getValue() {
		return value;
	}

	public Map<String, Object> getTrade() {
		return trade;
	}
}
